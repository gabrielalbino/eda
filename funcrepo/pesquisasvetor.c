#include "util.c"

/*Sintese:
 * Objetivo: Procurar um numero em um vetor de inteiros pelo método binário
 * Parâmetros: Vetor, tamanho do vetor  (ultimo elemento + 1), numero a ser procurado
 * Retorno: Índice que o numero se encontra no vetor
*/

int iProcuraBinaria(int* vetor, int fim, int num){
	int inicio = 0, encontrado = 0, mayExist = 1, media, indice;
	fim--;
	while(!encontrado && inicio <= fim){
		media = (inicio+fim)/2;
		if(vetor[media] > num){
			fim = media-1;
		}
		else if (vetor[media] < num){
			inicio = media+1;
		}
		else{
			indice = media;
			encontrado = 1;
		}	
		if(inicio == fim && !encontrado){
			indice = -1;
		}

	}
	return indice;
}


int confereVizinhanca(int* vetor, int numero, int fim, int posicao, int* indice){
	int aux, contador = 0;
	indice = NULL;
	for(aux = posicao; aux >= 0; aux--){
		if(vetor[aux] == numero){
			indice = realloc(indice, (++contador)*sizeof(int));
			if(!indice){
				printf("Falha ao alocar memoria, libere memoria e tente novamente");
				pause();
				return 0;
			}
			indice[contador-1] = aux;
		}
	}
	for(aux = posicao+1; aux <= fim; aux++){
		if(vetor[aux] == numero){
			indice = realloc(indice, (++contador)*sizeof(int));
			if(!indice){
				printf("Falha ao alocar memoria, libere memoria e tente novamente");
				pause();
				return 0;
			}
			indice[contador-1] = aux;
		}
	}
	return contador;
}

/*Sintese:
 * Objetivo: Procurar o intervalo que possui o numero em um vetor de inteiros pelo método binário (utilzizado caso haja numeros repetidos no vetor)
 * Parâmetros: (in) Vetor, tamanho do vetor  (ultimo elemento + 1), numero a ser procurado | (out) qtd de indices localizados
 * Retorno: Vetor de indices que se encontram no vetor
*/

int* iProcuraBinariaIntervalo(int* vetor, int fim, int num, int* qtdDeNumeros){
	int inicio = 0, encontrado = 0, mayExist = 1, media;
       	int* indice = 0;
	
	fim--;
	while(!encontrado && inicio <= fim){
		media = (inicio+fim)/2;
		if(vetor[media] > num){
			fim = media-1;
		}
		else if (vetor[media] < num){
			inicio = media+1;
		}
		else{
			encontrado = 1;
			*(qtdDeNumeros) = confereVizinhanca(vetor, num, fim, media, indice);
		}	

	}
	return indice;
}
/*Sintese:
 * Objetivo: Procurar um numero em um vetor de inteiros pelo método sequencial
 * Parâmetros: Vetor, tamanho do vetor  (ultimo elemento + 1), numero a ser procurado
 * Retorno: Índice que o numero se encontra no vetor
*/

int iProcuraSequencial(int* vetor, int tamanhoVetor, int num){
	int indice = -1, auxiliar;
	for(auxiliar = 0; auxiliar < tamanhoVetor; auxiliar++){
		if(vetor[auxiliar] == num){
			indice = auxiliar;
		}
	}
	return indice;
}
