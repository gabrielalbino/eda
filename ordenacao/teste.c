#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "validacoes.c"
#include "bubblesort.c"
#include "pesquisasvetor.c"
#include "util.c"
#define TAM_VETOR 8

int main(){
	void lerVetor(int* vetor);
	void printVetor(int* vetor);

	int vetor[TAM_VETOR];
	int opcao = 0, numero, indice, qtdNumeros;
	int* indiceVetor;
	do{
		printf("\n\nEscolha uma opção:\n");
		printf("0 - Sair\n");
		printf("1 - Alimentar vetor\n");
		printf("2 - Ordenar vetor\n");
		printf("3 - Consultar com pesquisa sequêncial\n");
		printf("4 - Consultar com pesquisa binária\n");
		printf("5 - Imprimir vetor\n");
		scanf("%d", &opcao);
		opcao = iValidaOpcao(opcao, 0, 5);
		switch(opcao){
			case 1:
				lerVetor(vetor);
				pause();
				break;
			case 2:
				iBubbleSort(vetor, TAM_VETOR);
				pause();
				break;
			case 3:
				scanf("%d", &numero);
				indice = iProcuraSequencial(vetor, TAM_VETOR, numero);
				if(indice >= 0){
					printf("O índice onde o numero %d se encontra é o: %d\n", numero, indice);
				}
				else
					printf("O numero %d não existe no vetor\n", numero);
				pause();
				break;
			case 4:
				scanf("%d", &numero);
				indiceVetor = iProcuraBinariaIntervalo(vetor, TAM_VETOR, numero, &qtdNumeros);
				if(qtdNumeros >= 0){
					printf("foram encantrados: %d numeros\n", qtdNumeros);
				}
				else
					printf("O numero %d não existe no vetor\n", numero);
				pause();
				break;
			case 5:
				printVetor(vetor);
				pause();
		}
	}while(opcao != 0);

}

void lerVetor(int* vetor){
	int aux;
	for(aux = 0; aux < TAM_VETOR; aux++){
		scanf("%d", &vetor[aux]);
	}
}

void printVetor(int* vetor){
	int aux;
	for(aux = 0; aux < TAM_VETOR; aux++){
		printf("%d ", vetor[aux]);
	}
	printf("\n");
}
