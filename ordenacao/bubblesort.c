void iBubbleSort(int* vetor, int elementos){
	int aux1, aux2;
	int temp;
	for(aux1 = 0; aux1 < elementos-1; aux1++){
		for(aux2 = aux1; aux2 < elementos; aux2++){
			if(vetor[aux2] < vetor[aux1]){
				temp = vetor[aux2];
				vetor[aux2] = vetor[aux1];
				vetor[aux1] = temp;
			}			
		}
	}
}

void fBubbleSort(float* vetor, int elementos){
	int aux1, aux2;
	float temp;
	for(aux1 = 0; aux1 < elementos-1; aux1++){
		for(aux2 = aux1; aux2 < elementos; aux2++){
			if(vetor[aux2] < vetor[aux1]){
				temp = vetor[aux2];
				vetor[aux2] = vetor[aux1];
				vetor[aux1] = temp;
			}			
		}
	}
}

