/*
Alunos
Gabriel Batista Albino Silva - 160028361
Rafael Makaha Gomes Ferreira - 160142369
*/

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define TAM_DESC 25
#define NRO_PRODUTOS 2

typedef struct{
	int codigo;
	int qtd;
	float precoUn;
	float precoTotal;
	char nome[TAM_DESC];
}infoProduto;

int main(){
	float lerInfoProduto(infoProduto* produtos);
	void mostrarInfoProdutos(infoProduto* produtos, float soma);	
	void bubbleSort(infoProduto* produtos);

	infoProduto produtos[NRO_PRODUTOS];
	float soma;	

	soma = lerInfoProduto(produtos);
	bubbleSort(produtos);
	mostrarInfoProdutos(produtos, soma);
	

}

float lerInfoProduto(infoProduto* produtos){
	void lerString(char* nome);
	float soma = 0.f;
	int count;
	for (count = 0; count < NRO_PRODUTOS; count++){
		scanf("%d", &produtos[count].codigo);
		lerString(produtos[count].nome);
		scanf("%d %f",&produtos[count].qtd, &produtos[count].precoUn);
		produtos[count].precoTotal = produtos[count].precoUn * produtos[count].qtd;
		soma+=produtos[count].precoTotal;
	}
}

void mostrarInfoProdutos(infoProduto* produtos, float soma){
        int count;
        printf("%6s %25s %15s %15s %15s\n", "Codigo","Descricao", "QTD", "Preco Un", "Preco Total");
	for (count = 0; count < NRO_PRODUTOS; count++){
                printf("%6d %25s %15d %15.2f %15.2f\n", produtos[count].codigo,produtos[count].nome,produtos[count].qtd, produtos[count].precoUn, produtos[count].precoTotal);
        }
	printf("Faturamento %68.2f\n\n", soma);
}

void bubbleSort(infoProduto* produtos){
	int count1, count2;
	infoProduto temp;	
	for(count1 = NRO_PRODUTOS-1; count1 > 0; count1--){
		for(count2 = 0; count2 < count1; count2++){
			if(produtos[count2].nome[0] > produtos[count2+1].nome[0]){
				temp = produtos[count2];
				produtos[count2]  = produtos[count2+1];
				produtos[count2+1] = temp;
			}
		}
	}
}

void lerString(char* desc){
	char c;
	int count = 0;
	c = getchar();
	if(c == '\n'){
		c = getchar();
	}
	while(c!= '\n' && count < TAM_DESC-1){
		c = tolower(c);
		desc[count] = c;
		desc[count+1] = '\0';
		count++;
		c = getchar();
	}
}
