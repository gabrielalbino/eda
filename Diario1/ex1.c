/*
Alunos:
Gabriel Batista Albino Silva - 160028361
Rafael Makaha Gomes Ferreira - 160142369
*/

#include <stdio.h>

typedef struct{
	int codigo;
	int qtd;
	float precoUn;
	float precoTotal;
}infoProduto;

#define NRO_PRODUTOS 100

int main(){
	float lerInfoProduto(infoProduto* produtos);
	void mostrarInfoProdutos(infoProduto* produtos, float soma);	
	void bubbleSort(infoProduto* produtos);

	infoProduto produtos[NRO_PRODUTOS];
	float soma;	

	soma = lerInfoProduto(produtos);
	bubbleSort(produtos);
	mostrarInfoProdutos(produtos, soma);
	

}

float lerInfoProduto(infoProduto* produtos){
	float soma = 0.f;
	int count;
	for (count = 0; count < NRO_PRODUTOS; count++){
		scanf("%d %d %f", &produtos[count].codigo,&produtos[count].qtd, &produtos[count].precoUn);
		produtos[count].precoTotal = produtos[count].precoUn * produtos[count].qtd;
		soma+=produtos[count].precoTotal;
	}
}

void mostrarInfoProdutos(infoProduto* produtos, float soma){
        int count;
        printf("%6s %15s %15s %15s\n", "Codigo", "QTD", "Preco Un", "Preco Total");
	for (count = 0; count < NRO_PRODUTOS; count++){
                printf("%6d %15d %15.2f %15.2f\n", produtos[count].codigo,produtos[count].qtd, produtos[count].precoUn, produtos[count].precoTotal);
        }
	printf("Faturamento %42.2f\n\n", soma);
}

void bubbleSort(infoProduto* produtos){
	int count1, count2;
	infoProduto temp;	
	for(count1 = NRO_PRODUTOS-1; count1 > 0; count1--){
		for(count2 = 0; count2 < count1; count2++){
			if(produtos[count2].codigo > produtos[count2+1].codigo){
				temp = produtos[count2];
				produtos[count2]  = produtos[count2+1];
				produtos[count2+1] = temp;
			}
		}
	}
}
