#include <stdio.h>
#include <stdlib.h>
#include "util.c"
#include "validacoes.c"
#define TAM_TURMA 40
#define TAM_NOME 30
typedef struct{
	char nome[TAM_NOME];
	int matricula;
}infoTurma;

int main(){
	void printTurma(infoTurma* turma, int lsup);
	int addAluno(infoTurma* turma, int lsup);
	int menu();
	
	int lsup = 1, opcao;
	infoTurma turma[TAM_TURMA];
	do{
		opcao = menu();
		switch(opcao){
		case 1:
			lsup = addAluno(turma, lsup);
			break;
		case 2:
			printTurma(turma, lsup);
		}
	}while(opcao != 0);


}

int menu(){
	int opcao;
	printf("Escolha uma opção:\n");
	printf("0 - Sair\n");
	printf("1 - Incluir alunos na turma\n");
	printf("2 - Imprimir turma\n");
	scanf("%d", &opcao);
	opcao = iValidaOpcao(opcao, 0, 2);
	return opcao;	
}


int addAluno(infoTurma* turma, int lsup){
	int continua; char c;
	do{
		clearBuffer();
		fgets(turma[lsup-1].nome, TAM_NOME, stdin);
		scanf("%d", &turma[lsup-1].matricula);
		lsup++;
		printf("Deseja cadastrar outro aluno? 1 para sim 0 para não\n");
		scanf("%d", &continua);
		continua = iValidaOpcao(continua, 0, 1);
	}while(continua == 1);
	return lsup;
}

void printTurma(infoTurma* turma, int lsup){
	int count;
	printf("%s\t\tNome\n", "Matricula");
	for(count = 0; count < lsup-1; count++){
		printf("%d\t\t%s", turma[count].matricula, turma[count].nome);
	}
}


