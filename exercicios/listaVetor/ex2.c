#include <string.h>
#include <stdio.h>
#include <stdlib.h>

struct dadosAluno{
	char nome[30];
	int mat;
	struct dadosAluno* next;
};

typedef struct dadosAluno dadosAluno;

void printAluno(dadosAluno* l){
	dadosAluno* p = l;
	int first = 1;
	if(p == NULL){
		printf("BASE VAZIA\n");
	}
	else{

		while(p!=NULL){
			if(!first){
				printf("; ");
			}
			else{
				first = 0;
			}
			printf("%s, %d", p->nome, p->mat);
			p = p->next;
		}
	printf("\n");
	}
	return;
}

void formatNome(char* nome){
	int aux;
	for(aux = 0; aux < strlen(nome); aux++){
		nome[aux] = nome[aux+1];
	}
	nome[aux-2] = '\0';
}

dadosAluno* addAluno(dadosAluno* l){
	dadosAluno* aluno = malloc(sizeof(dadosAluno));;
	scanf("%29[^123456789] %d",aluno->nome, &aluno->mat);
	aluno->next = NULL;
	formatNome(aluno->nome);
	getchar(); //limpa o \n
	if(l == NULL){
		l = aluno;
	}
	else{
		dadosAluno *p = l, *q=NULL;
		while(p!=NULL){
			q = p;
			p = p->next;
		}
		q->next = aluno;
	}
	printAluno(l);
	return l;
}

dadosAluno* rmAluno(dadosAluno* l){
	dadosAluno * aluno = malloc(sizeof(dadosAluno));
	scanf("%[^123456789] %d", aluno->nome, &aluno->mat);
	formatNome(aluno->nome);
	getchar();
	dadosAluno *p = l;
	dadosAluno *q = 0;
	while(p != NULL && p->mat != aluno->mat){
		q = p;
		p = p->next;
	}
	if(p == l){
		q = l;
		l = l->next;
		free(q);	
		printAluno(l);
	}
	else if(p == NULL){
		printf("ARGUMENTO INVÁLIDO\n");
	}
	else{
		q->next = p->next;
		free(p);
		printAluno(l);
	}

	return l;
}
int main(){
	dadosAluno* l = NULL;
	char op[3];
	int sair = 0;
	while(!sair){
		fgets(op, 3, stdin);
		if(strstr(op, "\n") != NULL);
		else if(strcmp(op, "IU") == 0){
			l = addAluno(l);			
		}
		else if(strcmp(op, "EX") == 0){
			l = rmAluno(l);
		}
		else if(strcmp(op, "QU") == 0){
			sair = 1;
		}
		else{
			printf("%s é uma opção inválida\n", op);
		}
	}
	return 0;
}
