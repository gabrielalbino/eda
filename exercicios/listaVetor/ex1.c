#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define TAM_NOME 30
#define TAM_OP 3

void recebeString(char* op, int tam){
	char buffer;
	int aux = 0;
	do{
		buffer = getchar();
		if(buffer == '\n' && aux == 0){
			buffer = getchar();
		}
		op[aux++]=buffer;
		if(buffer == ' ' || buffer == '\n'){
			op[--aux] = '\0';
		}
	}while(buffer != ' ' && aux < tam);	
}

typedef struct{
	char nome[TAM_NOME];
	int matricula;
}infoAluno;

infoAluno recebeAluno(){
	infoAluno buffer;
	char szBuffer[100];
	fgets(szBuffer, 100, stdin);
	int aux = 0;
	do{
		buffer.nome[aux] = szBuffer[aux];
		aux++;
	}while(szBuffer[aux+1] < '0' || szBuffer[aux+1] > '9');
	buffer.nome[aux] = '\0';
	buffer.matricula = atoi(szBuffer+aux+1);
	return buffer;
}

int excluiElemento(infoAluno elem, infoAluno* array, int lsup){
	void printAlunos(infoAluno* aluno, int lsup);
	int aux = 0;
	while(aux < lsup && array[aux].matricula != elem.matricula){
		aux++;
	}
	if(aux == lsup){
		printf("ENTRADA INVALIDA\n");
		return lsup;
	}
	else{
		while(aux < lsup-1){
			array[aux] = array[aux+1];
			aux++;
		}
	}
	lsup--;
	printAlunos(array,lsup);
	return lsup;
}

int adcionaElemento(infoAluno elem, infoAluno* array, int lsup){
	void printAlunos(infoAluno* aluno, int lsup);
	array[lsup] = elem;
	printAlunos(array, ++lsup);
	return lsup;
}

void printAlunos(infoAluno* aluno, int lsup){
	int aux;
	//printf("lsup = %d\n", lsup);
	if(lsup == 0){
		printf("BASE VAZIA\n");
		return;
	}
	for(aux = 0; aux < lsup; aux++){
		if(aux != 0){
		printf("; ");
		}
		printf("%s, %d", aluno[aux].nome, aluno[aux].matricula);
	}
	printf("\n");
}

int main(){
	infoAluno alunos[40];
	infoAluno buffer;
	int lsup = 0;
	char op[TAM_OP];
	do{
		recebeString(op, TAM_OP);
		buffer = recebeAluno();
		if(strcmp(op, "EX") == 0){
			lsup = excluiElemento(buffer, alunos, lsup);
		}	
		else if(strcmp(op, "IU") == 0){
			lsup = adcionaElemento(buffer, alunos, lsup);
		}
		else if (strcmp(op, "Q") == 0){}
		else{
			printf("ENTRADA INVÁLIDA\n");
		}
	}while(strcmp(op, "Q") != 0);
}
