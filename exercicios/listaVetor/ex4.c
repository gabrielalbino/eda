#include <stdlib.h>
#include <stdio.h>
#include <string.h>

struct infoAluno{
	char nome[30];
	int mat;
	struct infoAluno* next;
};

typedef struct infoAluno infoAluno;

void printAluno(infoAluno* l){
	infoAluno *p;
	int first = 1;
	p = l;
	if(p == NULL){
		printf("BASE VAZIA");
	}
	else{
		while(p!=NULL){
			if(!first) printf("; ");
			else first = 0;
			printf("%s, %d", p->nome, p->mat);
			p = p->next;
		}
		printf("\n");
	}
}
void filtrarNome(char* nome){
	int aux;
	for(aux = 0; aux < strlen(nome); aux++){
		nome[aux] = nome[aux+1];
	}
	nome[aux-2] = '\0';
}

infoAluno* addAluno(infoAluno* l){
	infoAluno* aluno = malloc(sizeof(infoAluno));
	aluno->next = NULL;
	scanf("%29[^0123456789] %d", aluno->nome, &aluno->mat);
	filtrarNome(aluno->nome);
	getchar();
	if(l == NULL){
		l = aluno;
	}
	else{
		infoAluno* p = l;
		infoAluno* q = NULL;
		while(p!= NULL && p->mat < aluno->mat){
			q = p;
			p = p->next;
		}
		if(p == NULL){
			q->next = aluno;
		}
		else if(p == l){
			aluno->next = l;
			l = aluno;
		}
		else{
			aluno->next = p;
			q->next = aluno;
		}

	}
	printAluno(l);
	return l;
}

infoAluno* exAluno(infoAluno *l){
	infoAluno* aluno = malloc(sizeof(infoAluno));
	aluno->next = NULL;
	scanf("%29[^0123456789] %d", aluno->nome, &aluno->mat);
	filtrarNome(aluno->nome);
	getchar();

	infoAluno *p = l, *q = NULL;
	while(p!=NULL && p->mat != aluno->mat){
		q = p;
		p = p->next;
	}
	if(q == NULL){
		q = l;
		l = l->next;
		printAluno(l);
		free(q);
	}
	else if(p == NULL){
		printf("PARAMETRO INVÁLIDO\n");
	}
	else{
		q -> next = p -> next;	
		free(p);
		printAluno(l);
	}
	return l;
}

int main(){
	infoAluno *l = NULL;
	char op[3];
	do{
		scanf("%2s", op);
		if(!strcmp(op, "IM")){
			l = addAluno(l);
		}
		else if(!strcmp(op, "EX")){
			l=exAluno(l);
		}
	
	}while(strcmp(op, "QU"));
}
