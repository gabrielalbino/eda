#include <string.h>
#include <stdio.h>
#include <stdlib.h>

struct infoAluno{
	int matricula;
	char nome[30];
};

typedef struct infoAluno infoAluno;

void printAlunos(infoAluno* alunos, int lsup){
	int aux;
	for(aux = 0; aux < lsup; aux++){
		if(aux != 0){
			printf("; ");
		}
		printf("%s, %d", alunos[aux].nome, alunos[aux].matricula);
	}
	printf("\n");
}

infoAluno lerDados(char* op){
	infoAluno buffer;
	char temp[100];
	fgets(temp, 100, stdin);
	int aux = 0;
	while(temp[aux] != ' '){
		op[aux] = temp[aux];
		aux++;
	}
	op[aux] = '\0';
	aux++;
	int aux2 = 0;
	while(temp[aux+1] < '0' || temp[aux+1] > '9'){
		buffer.nome[aux2] = temp[aux];
		aux2++; aux++;
	}
	buffer.nome[aux2] = '\0';
	aux++;
	buffer.matricula = atoi(temp+aux);
	return buffer;
}

int addAluno(infoAluno* alunos, infoAluno newAluno, int lsup){
	if(lsup == 0){
		*alunos = newAluno;
		printAlunos(alunos, ++lsup);
		return lsup;
	}
	int aux=0, aux2;
	while(alunos[aux].matricula < newAluno.matricula && aux < lsup){
		aux++;
	}
	if(aux == lsup){
		*(alunos+lsup) = newAluno;
		printAlunos(alunos, ++lsup);
		return lsup;
	}
	for(aux2 = lsup; aux2 >= aux; aux2--){
		alunos[aux2] = alunos[aux2-1];
	}
	alunos[aux] = newAluno;
	printAlunos(alunos, ++lsup);
	return lsup;
}

int removeAluno(infoAluno* alunos, infoAluno removedOne, int lsup){
	if (lsup == 1){
		printf("BASE VAZIA\n");
		return --lsup;
	}
	int aux = 0, aux2;
	while(alunos[aux].matricula != removedOne.matricula && aux < lsup){
		aux++;
	}
	if(aux == lsup){
		printf("ENTRADA INVÁLIDA\n");
		return lsup;
	}
	for(aux2 = aux; aux2 < lsup; aux2++){
		alunos[aux2] = alunos[aux2+1];
	}
	printAlunos(alunos, --lsup);
	return lsup;
}
int main(){
	struct infoAluno alunos[30];
	struct infoAluno buffer;
	int lsup = 0;
	char op[3];
	do{
		buffer = lerDados(op);
		if(strcmp(op, "IM") == 0){
			lsup = addAluno(alunos, buffer, lsup);
		}
		else if(strcmp(op, "EX") == 0){
			lsup = removeAluno(alunos, buffer, lsup);	
		}
		else{
			printf("OPÇÃO INVÁLIDA!");
		}
	}while(lsup < 30);
}
