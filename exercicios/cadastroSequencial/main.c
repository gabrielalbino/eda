#include "util.c"
#include "validacoes.c"
#include <stdio.h>
#include <stdlib.h>
#include "pesquisasvetor.c"

#define NRO_PESSOAS 40
#define TAM_NOME 30

typedef struct{
	int matricula;
	char nome[TAM_NOME];
}infoPessoa;

int main(){
	int lerPessoa(infoPessoa* pessoas, int lsup);
	void mostrarPessoas(infoPessoa* pessoas, int lsup);
	int deletarPessoa(infoPessoa* pessoas, int lsup);
	int menu();

	int opcao, lsup = 1;	
	infoPessoa pessoas[NRO_PESSOAS];
	
	do{
		opcao = menu();
		
		switch(opcao){
			case 1:
				lsup = lerPessoa(pessoas, lsup);
				break;
			case 2:
				lsup = deletarPessoa(pessoas, lsup);
				break;
			case 3:
				mostrarPessoas(pessoas, lsup);

		}
		pause();
		
	}while(opcao != 0);
	

	return 0;
}
/*Sintese:
	Objetivo: Mostrar o menu e obter a resposta do usuario
	Parâmetros: Nenhum
	Retorno: Opção do usuario
*/
int menu(){
	int opt;
	printf("\n\n\nOpções:\n");
	printf("1 - Cadastrar pessoas\n");
	printf("2 - Deletar pessoa\n");
	printf("3 - Mostrar pessoas\n");
	printf("0 - Sair\n");
	scanf("%d", &opt);
	return iValidaOpcao(opt, 0, 3);
}

int lerPessoa(infoPessoa* pessoas, int lsup){
	void encaixaPessoa(infoPessoa pessoa, infoPessoa* vetorPessoas, int lsup);

	int continua;
	infoPessoa pessoaTemp;
	do{
		printf("Insira a matricula: ");
		scanf("%d", &pessoaTemp.matricula);
		clearBuffer();
		printf("Insira o nome: ");
		fgets(pessoaTemp.nome, TAM_NOME, stdin);
		printf("Deseja adcionar outra pessoa?\n1 = sim\t\t2 = não\n");
		encaixaPessoa(pessoaTemp, pessoas, lsup);	
		scanf("%d", &continua);
		lsup++;
	}while(iValidaOpcao(continua, 1, 2) == 1 && lsup <= NRO_PESSOAS);
	return lsup;
}

void encaixaPessoa(infoPessoa pessoa, infoPessoa* vetorPessoas, int lsup){
	void aumentaPosicao(infoPessoa* pessoas, int start, int end);
	int aux;
	if(lsup == 1){
		vetorPessoas[0] = pessoa;
	}
	else{
		for(aux = 0; aux < lsup; aux++){
			if(vetorPessoas[aux].matricula > pessoa.matricula){
				aumentaPosicao(vetorPessoas, aux, lsup);
				vetorPessoas[aux] = pessoa;
				break;
			}			
		}
	}
}	


void aumentaPosicao(infoPessoa* pessoas, int start, int end){
	while(end > start){
		pessoas[end] = pessoas[end-1];
	//	printf("%d recebendo %d\n", end, end-1);
		end--;
	}
}

void mostrarPessoas(infoPessoa* pessoas, int lsup){
	int aux;
	printf("Matricula:\tNome:\n");
	for(aux = 0; aux < lsup-1; aux++){
		printf("%d\t%s", pessoas[aux].matricula, pessoas[aux].nome);
	}
}

int procuraBinariaMatricula(infoPessoa* vetor, int fim, int num){
        int inicio = 0, encontrado = 0, mayExist = 1, media, indice;
        fim--;
        while(!encontrado && inicio <= fim){
                media = (inicio+fim)/2;
                if(vetor[media].matricula > num){
                        fim = media-1;
                }
                else if (vetor[media].matricula < num){
                        inicio = media+1;
                }
                else{
                        indice = media;
                        encontrado = 1;
                }
                if(inicio == fim && !encontrado){
                        indice = -1;
                }

        }
        return indice;
}



int deletarPessoa(infoPessoa* pessoas, int lsup){
	int procuraBinariaMatricula(infoPessoa* vetor, int lsup, int num);
	void abaixarPosicao(infoPessoa* pessoas, int start, int end);
	int matricula, indice;

	printf("Digite a matricula de quem você deseja excluir: ");
	scanf("%d", &matricula);
	indice = procuraBinariaMatricula(pessoas, lsup, matricula);
	if(indice != -1){
		abaixarPosicao(pessoas, indice, lsup);
		lsup--;
	}
	else{
		printf("Matricula não está cadastrada!\n");
	}
	return lsup;
}

void abaixarPosicao(infoPessoa* pessoas, int start, int end){
        while(start < end-1){
                pessoas[start] = pessoas[start+1];
                start++;
        }

}


