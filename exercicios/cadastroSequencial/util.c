#ifndef UTIL
#define UTIL

#include <stdio.h>

/*Sintese:
 * Objetivo: Limpar o buffer da entrada padrão
 * Parâmetros: Nenhum
 * Retorno: Nenhum
*/

void clearBuffer(){
	char c;
	while ((c = getchar()) != '\n' && c != EOF) { }	
}

void pause(){
	char c;
	clearBuffer();
	printf("Pressione enter para continuar");
	c = getchar();
}

#endif
