#include <stdio.h>

#define TAMANHOVETOR 100

typedef struct{
	int matricula;
}dados;

int validaOpcao(int opt, int min, int max){
	while(opt < min || opt > max){
		printf("Valor inválido, insira-o novamente: ");
		scanf("%d", &opt);
	}
	return opt;
}

int menu(){
	int opt;
	printf("Escolha: \n");
	printf("0- sair\n");
	printf("1- Inserir valor no vetor\n");
	printf("2- Remover valor do vetor\n");
	printf("3- Imprimir vetor\n");
	scanf("%d", &opt);
	validaOpcao(opt, 0, 3);
}

int insereValorOrdenado(dados* vetor, int lsup, int argc){
	int aux, aux2, buffer, alocado = 0;
	printf("Insira o valor que deseja inserir: ");
	scanf("%d", &buffer);
	if(lsup == 0){
		if(argc>1) printf("primeiro indice, inserindo na primeira posição");
		vetor[0].matricula = buffer;
	}
	if(lsup == 101){
		printf("O vetor está cheio\n");
		return lsup;
	}
	for(aux = 0; aux < lsup; aux++){
		if(argc > 1) printf("analisando indice %d\n", aux);
		if(vetor[aux].matricula > buffer && !alocado){
			if(argc>1) printf("%d < %d", vetor[aux].matricula, buffer);
			for(aux2 = lsup; aux2 >= aux; aux2++){	
				vetor[aux2] = vetor[aux2-1];
			}
			vetor[aux].matricula = buffer;
			alocado = 1;
			
		}
	}
	if(!alocado){
		vetor[lsup].matricula = buffer;
	}
	return ++lsup;
}

void imprimirVetor(dados* vetor, int lsup){
	int aux;
	for(aux = 0; aux < lsup; aux++){
		printf("%d\n", vetor[aux].matricula);
	}
}

int main(int argc, char** argv){
	int opt;
	int lsup = 0;
	dados vetor[TAMANHOVETOR];
	do{
		opt = menu();
		switch(opt){
			case 1:
				lsup = insereValorOrdenado(vetor, lsup, argc);
				break;
			case 2:
				break;
			case 3:
				imprimirVetor(vetor, lsup);
		}

	}while(opt != 0);
	return 0;
}
