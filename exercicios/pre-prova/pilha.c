#include <stdio.h>
#include <stdlib.h>

int main(){
	int soma = 0, pilha[6], topo = 0,aux = 0, nroCaixas, temp;
	scanf("%d", &nroCaixas);
	while(aux < nroCaixas){
		scanf("%d", &temp);
		if(topo == 0 || temp <= pilha[topo-1]){
			pilha[topo++] = temp;
		}
		aux++;
	}
	while(topo > 0){
		aux = pilha[--topo];
		printf("%d-%d ",topo, aux);
		soma += aux;
	}
	printf("\n%d", soma);
}
