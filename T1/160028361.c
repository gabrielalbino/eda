//ALUNO: GABRIEL BATISTA ALBINO SILVA 16/0028361

/*
**Includes
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
/*
**Defines
*/
#define TAMMENU 50

/*
**Struct para controlar os processos
*/
typedef struct processo{
  char tipo;
  char tag;
  int tempo;
  int tamanho;
  int endereco;
  struct processo* proximo;
  struct processo* anterior;
}processo;

/*
**Struct de cabeçalho
*/
typedef struct cabecalho{
  int memoriaOcupada;
  int tamanhoMemoria;
  int memoriaLivre;
  struct processo* inicio;
  struct processo* final;
}cabecalho;

/*Sintese:
**Objetivo: SIMULAR A MEMÓRIA DE UM COMPUTADOR UTILIZANDO LISTAS DUPLAMENTE ENCADEADA E VETORES
**Entrada: Tamanho do processo, nome do processo, opção do menu, tamanho da memória
**Saída: Exibição da memória do computador
*/
int main(){
  //declarações
  int opt;
  processo temporario;
  int lerListaDeArquivo(cabecalho* p);
  void salvaListaEmArquivo(cabecalho*p);
  cabecalho* criarCabecalho();
  void recebeInformacoesIniciais(cabecalho* header);
  void criarNovoProcesso(cabecalho* header, char tipo, char tag, int tempo, int tamanho);
  int menu();
  void imprimeMemoria(cabecalho* header);
  void clearBuffer();
  void avancaMinuto(cabecalho* p);
  void limparMemoria(cabecalho* p);
  void  atualizaEnderecos(cabecalho* p);
  void imprimelista(cabecalho* l);
  void atualizaEspacoLivre(cabecalho* header);
  void pause();
  void desalocaLista(processo* p, processo* inicio);
  //instruções
  srand (time(NULL));
  cabecalho* header = criarCabecalho();
  if(!lerListaDeArquivo(header)){
    recebeInformacoesIniciais(header);
    criarNovoProcesso(header, 'h', 0, 0, header->tamanhoMemoria);
  }
  do{
    atualizaEspacoLivre(header);
    imprimeMemoria(header);
    opt = menu();
    switch(opt){
      case 1:
        system("clear");
        clearBuffer();
        printf("Insira a sigla do processo (Uma letra): ");
        scanf("%c", &temporario.tag);
        printf("Insira o tamanho do processo (Em Kbytes): ");//validar
        scanf("%d", &temporario.tamanho);
        temporario.tempo = rand() % 5 + 1;
        criarNovoProcesso(header, 'p', temporario.tag, temporario.tempo, temporario.tamanho);
        break;
      case 2:
        avancaMinuto(header);
    }
    atualizaEspacoLivre(header);
    atualizaEnderecos(header);
    limparMemoria(header);
  }while(opt != 3);
  salvaListaEmArquivo(header);
  desalocaLista(header->inicio, header->inicio);
  free(header);
}

/*Sintese:
**Objetivo: Avançar 1 minuto no tempo dos processos
**Parâmetros: cabeçalho
**Retorno: nenhum
*/
void avancaMinuto(cabecalho* p){
  processo* l = p->inicio;
  do{
    if(l->tipo == 'p')
      l->tempo = (l->tempo)-1;
    l=l->proximo;
  }while(l != p->inicio);
}

/*Sintese:
**Objetivo: Atualizar o endereço de memoria dos processos existente
**Parâmetros: cabeçalho
**Retorno: nenhum
*/
void  atualizaEnderecos(cabecalho* p){
  processo* l = p->inicio;
  void pause();
  do{
    if(l == p->inicio){
      l->endereco = 0;
    }
    else{
      l->endereco = l->anterior->endereco + l->anterior->tamanho;
    }
    l = l->proximo;
  }while(l != p->inicio);
}

/*Sintese:
**Objetivo: Limpar a memoria do computador (desocupar processos terminados ou heaps vazios)
**Parâmetros: cabeçalho
**Retorno: nenhum
*/
void limparMemoria(cabecalho* p){
  void imprimeMemoria(cabecalho* header);
  void pause();
  void finalizaProcessosAcabados(cabecalho* p);
  void fundirHeaps(cabecalho* p);
  void removerHeapsNulos(cabecalho* p);
  int memoriaEstaLimpa(cabecalho* p);
  finalizaProcessosAcabados(p);
  fundirHeaps(p);
  removerHeapsNulos(p);
  if(!memoriaEstaLimpa(p)){
    limparMemoria(p);
  }
}

/*Sintese:
**Objetivo: verificar se a memoria esta limpa
**Entrada: cabeçalho
**Saída: 1 para memoria limpa, 0 para fragmentos na memoria
*/
int memoriaEstaLimpa(cabecalho* p){
  processo* l;
  int contadorDeHeaps = 0;
  l = p->inicio;
  do{
    if(l->tipo == 'h'){
      contadorDeHeaps++;
      if(l->tamanho == 0){
        return 0;
      }
    }
    else{
      contadorDeHeaps = 0;
      if(l->tempo == 0){
        return 0;
      }
    }
    if(contadorDeHeaps > 1){
      return 0;
    }
    l=l->proximo;
  }while(l != p->inicio);
  return 1;
}

/*Sintese:
**Objetivo: transformar processos termnados em heap
**Entrada: cabeçalho
**Saída: nenhum
*/
void finalizaProcessosAcabados(cabecalho* p){
  processo* l;
  void pause();
  l = p->inicio;
  do{
    if(l->tipo == 'p' && l->tempo == 0){
      l->tipo = 'h';
    }
    l=l->proximo;
  }while(l != p->inicio);
}

/*Sintese:
**Objetivo: transformar um processo em heap quando o seu tempo acaba
**Entrada: cabeçalho
**Saída: nenhum
*/
void fundirHeaps(cabecalho* p){
  processo* l;
  l = p->inicio;
  do{
    if(l->tipo == 'h'){
      if(l->proximo->tipo == 'h' && l != p->final){
        l->tamanho+= l->proximo->tamanho;
        l->proximo->tamanho = 0;
      }
    }
    l = l->proximo;
  }while(l != p->inicio);
}

/*Sintese:
**Objetivo: remover heaps nulos
**Entrada: cabeçalho
**Saída: nenhum
*/
void removerHeapsNulos(cabecalho* p){
  processo *l, *backup;
  l = p->inicio;
  do{
    backup = l->proximo;
    if(l->tipo == 'h' && l->tamanho == 0){
      if(p->inicio == l){
        p->inicio = l->proximo;
      }
      else if(p->final == l){
        p->final = l->anterior;
      }
      l->anterior->proximo = l->proximo;
      l->proximo->anterior = l->anterior;
      free(l);
    }
    l = backup;
  }while(l != p->inicio);
}


/*Sintese:
**Objetivo: Criar um cabeçalho
**Entrada: nenhum
**Saída: cabeçalho
*/
cabecalho* criarCabecalho(){
  cabecalho* novoCabecalho = malloc(sizeof(cabecalho));
  novoCabecalho->memoriaOcupada = 0;
  novoCabecalho->tamanhoMemoria = 0;
  novoCabecalho->memoriaLivre = 0;
  novoCabecalho->inicio = NULL;
  novoCabecalho->final = NULL;
  return novoCabecalho;
}

/*Sintese:
**Objetivo: Receber informações iniciais da simulação
**Entrada: cabeçalho
**Saída: nenhum
*/
void recebeInformacoesIniciais(cabecalho* header){
  system("clear");
  printf("Insira o tamanho da memoria em KBs: ");
  scanf("%d", &header->tamanhoMemoria);
  header->memoriaLivre = header->tamanhoMemoria;
  system("clear");
}

/*Sintese:
**Objetivo: Criar um novo processo
**Entrada: cabeçalho, tipo, tag, tempo e tamanho do processo
**Saída: nenhum
*/
void criarNovoProcesso(cabecalho* header, char tipo, char tag, int tempo, int tamanho){
  //declarações
  void atualizaEspacoLivre(cabecalho* header);
  void adcionaProcessoNaLista(processo* novo, cabecalho* header);
  processo* novo = malloc(sizeof(processo));
  void pause();
  //instruções
  novo->tipo = tipo;
  novo->tag = tag;
  novo->tamanho = tamanho;
  novo->tempo = tempo;
  novo->endereco = 0;
  if(novo->tipo == 'h' || header->memoriaLivre >= novo->tamanho){
      adcionaProcessoNaLista(novo, header);
  }
  else{
    printf("Não há espaço suficiente na memória para alocar esse processo!\n");
    pause();
  }
}

/*Sintese:
**Objetivo: Adcionar um processo na lista encadeada
**Entrada: processo, cabeçalho
**Saída: nenhum
*/
void adcionaProcessoNaLista(processo* novo, cabecalho* header){
  void atualizaEspacoLivre(cabecalho* header);
  void reorganizaMemoria(cabecalho* header);


  int alocado = 0;
  void pause();
  processo *p;
  if(header->inicio == NULL){
    header->inicio = novo;
    header->final = novo;
    novo->proximo = novo;
    novo->anterior = novo;
    alocado = 1;
  }
  else{
    p = header->inicio;
    do{
      if(p->tipo == 'h' && p->tamanho >= novo->tamanho){
        p->tamanho -= novo->tamanho;
        novo->anterior = p-> anterior;
        novo->proximo = p;
        p->anterior->proximo = novo;
        p->anterior = novo;
        if(p == header->inicio){
          header->inicio = novo;
        }
        alocado = 1;
        break;
      }
      p = p->proximo;
    }while(p != header->inicio);
    if(!alocado){
      reorganizaMemoria(header);
      adcionaProcessoNaLista(novo, header); //recursividade
    }
  }
}

/*Sintese:
**Objetivo: reorganizar a memoria do computador (juntar os heaps para abrir espaço para um novo processo)
**Parâmetros: header
**Retorno: Nenhum
*/
void reorganizaMemoria(cabecalho* header){
  void limparMemoria(cabecalho* p);
  void atualizaEspacoLivre(cabecalho* header);
  processo* p, *ultimoHeap;
  p = header->inicio;
  do{
    if(p->tipo == 'h'){
      ultimoHeap = p;
    }
    p=p->proximo;
  }while(p!=header->inicio);
  p = header->inicio;
  do{
    if(p->tipo == 'h' && p!=ultimoHeap){
      ultimoHeap->tamanho+=p->tamanho;
      p->tamanho = 0;
    }
    p=p->proximo;
  }while(p!=header->inicio);
  limparMemoria(header);
}

/*Sintese:
**Objetivo: Atualizar o cabeçalho com o valor da memoria livre atual
**Parâmetros: header
**Retorno: Nenhum
*/
void atualizaEspacoLivre(cabecalho* header){
  processo* p;
  header->memoriaLivre = 0;
  p = header->inicio;
  do{
    if(p->tipo == 'h'){
        header->memoriaLivre += p->tamanho;
    }
    p=p->proximo;
  }while(p!=header->inicio);
}

/*Sintese:
**Objetivo:Exibir um menu para o usuário e recolher a opção do menu
**Parâmetros: Nenhum
**Retorno: Opção do usuário
*/
int menu(){
  int o;
  printf("Escolha uma opção:\n");
  printf("1 - Criar um processo\n");
  printf("2 - Passar 1 minuto\n");
  printf("3 - Sair do programa\n");
  scanf("%d", &o);
  return o;
}

/*Sintese:
**Objetivo: Imprimir a memoria do computador
**Parâmetros: cabeçalho, memoria
**Retorno: Nenhum
*/
void imprimeMemoria(cabecalho* header){
  void alinhaString(char* string);
  int i, j;
  system("clear");
  processo* procAtual;
  char string[TAMMENU], info[TAMMENU];
  printf("╒");
  for(i = 0; i < TAMMENU-1; i++)
    printf("═");
  printf("╕\n");
  snprintf(string, TAMMENU, "Memoria | Espaco livre: %d", header->memoriaLivre);
  alinhaString(string);
  printf("│%s│\n", string);
 printf("╞");
  for(i = 0; i < TAMMENU-1; i++)
    printf("═");
  printf("╡\n");
  procAtual = header->inicio;
  do{
        if(procAtual->tipo == 'p'){
        snprintf(string, TAMMENU, "Processo %c: Ocupa %d Kbytes",procAtual->tag, procAtual->tamanho);
        snprintf(info, TAMMENU, "%d minutos restantes, Endereco na memoria: %d",procAtual->tempo, procAtual->endereco);
        alinhaString(string);
        alinhaString(info);
        printf("│%s│\n", string);
        printf("│%s│\n", info);
        }
        else{
          snprintf(string, TAMMENU, "Espaco Vazio: %d Kbytes", procAtual->tamanho);
          snprintf(info, TAMMENU, "Endereco na memoria: %d", procAtual->endereco);
          alinhaString(string);
          alinhaString(info);
          printf("│%s│\n", string);
          printf("│%s│\n", info);
        }
        if(procAtual->proximo != header->inicio){
          printf("├");
           for(j = 0; j < TAMMENU-1; j++)
             printf("─");
           printf("┤\n");
        }
        else{
          printf("╞");
          for(j = 0; j < TAMMENU-1; j++)
             printf("═");
          printf("╡\n");
        }
        procAtual = procAtual->proximo;
    }while(procAtual != header->inicio);
  strcpy(string, "Fim da memoria");
  alinhaString(string);
  printf("│%s│\n", string);
  printf("╘");
  for(i = 0; i < TAMMENU-1; i++)
      printf("═");
  printf("╛\n\n");
}

/*Sintese:
**Objetivo: Deixar uma string com 50 caracteres
**Parâmetros: string
**Retorno: nenhum
*/
void alinhaString(char* string){
  for(int i = strlen(string); i < TAMMENU-1; i++)
	string[i] = ' ';
  string[TAMMENU-1] = '\0';
}

/*Sintese:
**Objetivo: Limpar o buffer da entrada
**Parâmetros: Nenhum
**Retorno: Nenhum
*/
void clearBuffer(){
	char c;
	while ((c = getchar()) != '\n' && c != EOF) { }
}

/*Sintese:
**Objetivo: Pausar o console até o usuario pressionar uma tecla
**Parâmetros: Nenhum
**Retorno: Nenhum
*/
void pause(){
	clearBuffer();
	printf("Pressione enter para continuar");
	getchar();
}

/*Sintese:
**Objetivo: salvar simuçlação atual em um arquivo
**Parâmetros: cabeçalho
**Retorno: nenhum
*/
void salvaListaEmArquivo(cabecalho*p){
  int opcao;
  printf("Deseja salvar a simulação? (1 ou 0)\n");
  scanf("%d", &opcao);
  if(!opcao){
    return;
  }
  FILE* pFile = fopen("dados.bin", "wb");
  fwrite((void*)p, sizeof(cabecalho), 1, pFile);
  processo* l = p->inicio;
  do{
    fwrite((void*)l, sizeof(processo), 1, pFile);
    l = l->proximo;
  }while(l!= p->inicio);
}

/*Sintese:
**Objetivo: ler dados de um arquivo
**Parâmetros: cabeçalho
**Retorno: nenhum
*/
int lerListaDeArquivo(cabecalho* header){
  header->tamanhoMemoria = 0;
  processo novo;
  int valido = 1;
  FILE* pFile = fopen("dados.bin", "rb");
  int opcao = 1;
  if(pFile != NULL){
    printf("Foi localizado um arquivo de simulação. Deseja carrega-lo? (1 ou 0)\n ");
    scanf("%d", &opcao);
    if(opcao){
      fread((void*)header, sizeof(cabecalho), 1, pFile);
      header->inicio = NULL;
      header->final = NULL;
      criarNovoProcesso(header, 'h', 0, 0, header->tamanhoMemoria);
      while(valido){
        valido = fread(&novo, sizeof(processo), 1, pFile);
        if(novo.tipo == 'h'){
          criarNovoProcesso(header, 'p', 0, 0, novo.tamanho);
        }
        if(novo.tipo == 'p'){
          criarNovoProcesso(header, 'p', novo.tag, novo.tempo, novo.tamanho);
        }
      }
    }
  }
  limparMemoria(header);
  if(opcao == 1){
    return 1;
  }
  return 0;
}

/*Sintese:
**Objetivo: Desalocar as variaveis dinâmicamente alocadas
**Parâmetros: cabeçalho
**Retorno: nenhum
*/
void desalocaLista(processo* p, processo* inicio){
  if(p->proximo != inicio)
    desalocaLista(p->proximo, inicio);
  free(p);
}
