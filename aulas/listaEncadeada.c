/*
As posições não ficam mais sequenciais na memória

A struct aponta para a proxima

Exemplo de algoritmo:
*/
#include <stdio.h>
#include <stdlib.h>
struct name{
	int info;
	struct name* prox;
};

struct name* lstCreate(){
	return NULL;
}

struct name* procuraLista(struct name* l, int value){
	struct name* p;
	p = l;
	while(p!=NULL){
		if(p->info == value){
			return p;
		}
		p = p->prox;
	}
	return NULL;
}
void imprimirLista(struct name* l){
	struct name* p;
	p = l;
	printf("Lista:");
	while(p != NULL){
		printf(" %d", p->info);
		p = p->prox;
	}
	printf("\n");
}
struct name* lstInsert(struct name* l, int valor){
	struct name* novo = (struct name*)malloc(sizeof(struct name));
	novo-> info = valor;
	novo-> prox = l;
	return novo;
}

struct name* lstInsertOrdenada(struct name* l, int valor){
	/*cria o novo elemento e atribui valor a ele*/
	struct name* novo = (struct name*)malloc(sizeof(struct name));
	novo->info =  valor;
	novo-> prox= NULL;
	
	struct name* p = l;
	if(p == NULL){
		l = novo;
	}
	else{
		while(p->prox != NULL){
			p = p->prox;
		}
		p -> prox = novo;
	}
	return l;
}
int menu(){
	int opt;
	printf("0 - sair\n");
	printf("1 - inserir elemento no inicio da lista encadeada\n");
	printf("2 - inserir elemento no final da lista encadeada\n");
	printf("3 - procurar por elemento na lista encadeada\n");
	printf("4 - imprimir a lista encadeada\n");
	printf("5 - desalocar a lista encadeada\n");
	scanf("%d", &opt);
	return opt;
}

void deleteLista(struct name* l){
	struct name* temp;
	do{
		temp = l->prox;
		free(l);
		l = temp;
	}while(l != NULL);
}

int main(){
	struct name* l;
	l = lstCreate();
	int opt, value;
	do{
		opt = menu();
		switch(opt){
			case 1:
				printf("Insira o numero que deseja inserir: ");
				scanf("%d", &value);
				l = lstInsert(l,value);
				break;
			case 2:
				printf("Insira o numero que deseja inserir: ");
				scanf("%d", &value);
				l = lstInsertOrdenada(l, value);
				break;
			case 3:
				printf("Insira o elemento que você quer pesquisar: ");
				scanf("%d", &value);
				printf("O valor %d está localizado em %p", value, procuraLista(l, value));
				break;
			case 4:
				imprimirLista(l);
				break;
			case 5:
				deleteLista(l);
				l = NULL;			
		}
	}while(opt != 0);
	return 0;
}
