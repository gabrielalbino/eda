/*
	Fila = estrutura de dados que admite inserção de novos elementos e remoção de elementos antigos utilizando FIFO (first in first out)
	a)Implementação simples em vetor:
	
		|0| |ini|#|#|fim| |n-1|

		obs:
		se ini == fim = lista vazia
		se fim == n = lista cheia
		p/ inserir -> fila[fim++]=x;
		p/ remover -> fila[ini++];

	b)Implementação de fila circular em vetor:

		| | |ini|#|#|fim| | |   |
		 0		     n-1

		|#|#|fim| | |ini|#|#|###|
		 0		     n-1

		obs:
		se ini == fim = fila vazia;
		se fim+1 == ini ou fim+1==n  ini == 0 -> fila cheia
		p/ inserir -> fila[fim++] = y;
			      se(fim==n) -> fim = 0;
		p/ remover -> fila[ini++];
			      se(ini==n) -> ini = 0;
		se(fim+1 == ini || fim+1 == n & ini == 0) = transbordando (sobreescrevendo)


	ex: simular uma lista de M posições de N inteiros com N<=M=6;
	-> inserir, remover, imprimir do inicio para o fim, imprimir o comprimento
	ex2:escrever um programa para simular o funcionamente de uma fila de N inteiros num vetor de M posições tal que N>M=6; considerar as mesmas funções do ex anterior
*/
#include <stdio.h>
#include <stdlib.h>

#define TAMFILA 6

int menu(){
	int o;
	printf("1- Inserir um elemento na fila\n");
	printf("2- Remover um elemento da fila\n");
	printf("3- Imprimir a fila do inicio para o fim\n");
	printf("4- Imprimir o comprimento da fila\n");
	printf("0- Sair\n");
	scanf("%d", &o);
	return o;
}

void imprimeComprimentoFila(){
	return;
}

int removeElementoFila(int* fila, int inicio, int fim){
	if(inicio!=fim){
		if(inicio == TAMFILA){
			inicio = 0;
		}
		else{
			inicio++;
		}
	}
	return inicio;
}

int insereElementoFila(int* fila, int* inicio, int fim){
	int x;
	printf("Insira o valor de X para inserir: ");
	scanf("%d", &x);
	if(fim == TAMFILA && *inicio == 0 || fim+1 == *inicio){
		if(*inicio == 6)
			*inicio = 0;
		*inicio += 1;	
	}
	if(fim == TAMFILA){
		fim = 0;
	}
	fila[fim++] = x;
	return fim;
}

void imprimeFila(int*fila, int inicio, int fim){
	int i;
	printf("\n\ninicio = %d\nfim = %d\n\n", inicio, fim);
	for(i = inicio; i!=fim; i++){
		if(i==TAMFILA){
			i = 0;
		}
		printf("%d - %d\n",i, fila[i]);
	}
	printf("\n\n");
}
int main(){
	int option;
	int fila[TAMFILA], inicio=0, fim=0;
	do{
		option = menu();
		switch(option){
			case 1:
				fim=insereElementoFila(fila, &inicio, fim);
				imprimeFila(fila, inicio, fim);
				break;
			case 2:
				inicio=removeElementoFila(fila, inicio, fim);
				imprimeFila(fila, inicio, fim);
				break;
			case 3:
				imprimeFila(fila, inicio, fim);
				break;
			case 4:
				imprimeComprimentoFila();
		}
	}while(option != 0);
}
