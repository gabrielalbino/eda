/*
ARVORE BALANCEADA
	-> PERMITE ACESSO A QUALQUER NÓ DÁ ÁRVORE
	-> A ALTURA DA SAE E A ALTURA DA SAD DE QUALQUER NÓ DIFEREM EM, NO MÁXIMO, UMA UNIDADE.
	-> Solução: Após uma operação (Inserção ou Remoção) numa arvore, realiza-se o balanceamento.
	-> Possibilidades:
		->1º opção: realizar um balanceamento estático: 
			-> copiar a árvore para um vetor e ordena-lo.
			-> o elemento no meio do vetor (N/2) será a raiz
			-> para cada lado do vetor repita o processo
		->2º opção: realizar um balanceamento dinâmico:
			-> trata-se dos movimentos de rotação, considerando o fator de balanceamento de cada nó da árvore.
			-> como identificar o local da rotação:
				-> A = Nó ancestral mais próximo do nó inserido/removido cujo o fator de balanceamento != 0 antes da operação.
				-> B = Filho da A na subárvore onde ocorreu a operação
				-> FB = ALTURA SAD - ALTURA SAE;
				-> SE FB > 1 = ROTATION LEFT
				-> SE FB < -1 = ROTATION RIGHT
				-> ALGUNS CASOS REQUEREM ROTAÇÃO DUPLA
-> Algoritmos:
*/
typedef struct no{
	struct no* esq;
	struct no* dir;
	int info;
}no;

/*RR:*/
void rot_dir(no* p){
	no *q, *temp;
	q = p->esq;
	temp = q->dir;
	q->dir = p;
	p->esq = temp;
	p = q;
}
/*RL:*/
void rot_esq(no* p){
	no *q, *temp;
	q = p->dir;
	temp = p->esq;
	q->esq = p;
	q->dir = temp;
	p = q;
}
/*RLR:*/
void rot_esq_dir(no* p){
	rot_esq(p->esq);
	rot_dir(p);
}
/*RRL:*/
void rot_dir_esq(no* p){
	rot_dir(p->dir);
	rot_esq(p);
}

int main(){
	return 0;
}
