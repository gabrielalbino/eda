/*
->ESTRUTURA DE DADOS NÃO LINEAR
->UMA ARVORE T É UM CONJUNTO INFINITO DE ELEMENTOS (NÓS OU VÉRTICES), TAIS QUE SE T = 0 A ARVORE ESTÁ VAZIA
->TODA LISTA NÃO VAZIA POSSUI:
1- UM NÓ ESPECIAL CHAMADO RAIZ
2- OS DEMAIS NÓS:
   * OU SÃO UM CONJUNTO VAZIO
   * SÃO DIVIDIDOS EM N>=1 CONJUNTOS DISJUNTOS NÃO VAZIO (T1, T2,..,Tn) DE ÁRVORES (SUB-ÁRVORES)
   * UM NÓ SEM SUB-ÁRVORES É DENOMINADO NÓ-FOLHA OU SOMENTE FOLHA
   * RELAÇÕES ENTER NÓS DE ÁRVORES
    ->SE X É A RAIZ DA ÁRVOORE E Y É RAIZ DO SUBÁRVORE DE X, ENTÃO X É PAI Y E Y É FILHO DE X
    ->X É ANCESTRAL DE Y (E Y É DESCENDENTE DE X) SE X 'PAI DE Y OU SE X É PAI DE ALGUM ANCESTRAL DE Y
    ->DOIS NÓS SÃO IRMÃO SE SÃO FILHOS DO MESMO PAI
    ->DOIS NÓS SÃO IRMÃOS SE SÃO FILHOS DO MESMO PAI->SE OS NÓS Y1,Y2,YJ SÃO IRMÃOS E O NÓ Z É FILHO DE Y1, ENTÃO Y2,...,YJ SÃO TIOS DE Z
3- CONCEITOS:
  * O NÍVEL DE UM NÓ X É DEFINIDO COMO:
    -> A RAIZ DE NÍVEL
    -> OS DEMAIS TEM NÍVEL = NÍVEL DO PAI + 1
  * OBS: OS NÓS FOLHA SÃO OS DE MAIOR NÍVEL
  * O GRAU DE UM NÓ X DA ÁRVORE ÉIGUAL AO Nº DE FILHOS DE X
  * O GRAU DA ÁRVORE T É O MAIOR ENTRE OS GRAUS DE TODOS OS SEUS NÓS
4- ARVORES BINÁRIAS
  * CADA NÓ TEM ZERO, UM OU DOIS FILHOS
  * DEFINIÇÃO: ÁRVORE BINÁRIA
    - UMA ARVORE VAZIA OU UM NÓ COM DUAS SUB-ÁRVORES:
	-SUB-ÁRVORE DA ESQUEDA(SAE)
	-SUB-ÁRVORE DA DIREITA(SAD)
    - IMPLEMENTAÇÃO DE ÁRVORE BINÁRIA:
    STRUCT ARV{
	CHAR INFO;
	STRUCT ARV* ESQ;
	STRUCT ARV* DIR;
    }

    EXERCÍCIO:
    1) CONSIDERE AS FUNÇÕES ARV_CRIA E ARV_CRIAVAZIA ABAIXO E MSTRE COMO FICA A CONSTRUÇÃO DA SEGUINTE ÁRVORE:

    A->B->D
     ->C->E
	->F

    ARV* ARV_CRIAVAZIA(){
	RETURN NULL;
    }

    ARV* ARV_CRIA(CHRA C, ARV*SAE, ARV*SAD){
	ARV* P;
	P = (ARV*)MALLOC(SIZEOF(ARV));
	P->INFO = C;
        P->ESQ = SAE;
        P->DIR = SAD;
	RETURN P;
    }

    2) CONSIDERE O DESENHO D ARVORE APRESENTADO NO EXERCICIO ATNERIOR E MONTE UM PROGRAMA COM AS SEGUINTES FUNÇÕES:
	0- SAIR
	1- CRIA ÁRVORE (PRÉ DEFINIDA)
	2- DESTROI A ARVORE
	3- CONSULTA ELEMENTO
	4- IMPRIME A ÁRVORE

    ARVORES BINÁRIAS DE BUSCA:
	->TRATA-SE DE UMA ARVORE BINÁRIA ORDENADA
	->A ORDENAÇÃO SEGUE O CRITÉRIO: SAE < NÓ < SAD
	->BUSCA:
		->COMPARE O ELEMENTO COM A RAIZ
		->SE FOR IGUAL PARE A BUSCA
		->SE FOR MENOR BUSCA NA SAE
		->SE FOR MAIOR BUSQUE NA SAD
*/

/*
  Universidade de Brasilia
  Filipe Dias - 16/0006163
  Gabriel Albino - 16/0028361
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct arv{
  char info;
  struct arv *esq;
  struct arv *dir;
  struct arv *prox;
}ARV;

ARV *arv_cria(char c, ARV *sae, ARV *sad){
  ARV *p;
  p=(ARV*)malloc(sizeof(ARV));
  p->info=c;
  p->esq=sae;
  p->dir=sad;
  return p;
}

int arv_vazia(ARV *p){
  return p==NULL;
}

ARV *arv_criavazia(){
  ARV *p=NULL;
    return p;
}

ARV *arv_destroi(ARV *a){
  if(!arv_vazia(a)){
    arv_destroi(a->esq);
    arv_destroi(a->dir);
    free(a);
  }
  return NULL;
}

void imprime_arvore_pre(ARV *p){
  if(p){
    printf("%c\n",p->info);
    imprime_arvore_pre(p->esq);
    imprime_arvore_pre(p->dir);
  }
}

void imprime_arvore_em(ARV *p){
  if(p){
    imprime_arvore_em(p->esq);
    printf("%c\n",p->info);
    imprime_arvore_em(p->dir);
  }
}

void imprime_arvore_pos(ARV *p){
  if(p){
    imprime_arvore_pos(p->esq);
    imprime_arvore_pos(p->dir);
    printf("%c\n",p->info);
  }
}

void imprime(ARV *p, int nivel){
     int i;
     if(p == NULL)
        return;
     imprime(p->dir, nivel+1);
     for(i=0;i<nivel;i++)
        printf("      ");
     printf("%6c\n\n",p->info);
     imprime(p->esq,nivel+1);

}

ARV* pesquisa_ARV(ARV* p, char c);

void imprime_porFila(ARV *a)
{
  char fila[11];
  char pilha[11];
  int inicio = 0;
  int fim = 0;
  int topo = 0;
  ARV* temp;
  fila[fim++] = a->info;
  while(inicio != fim)
  {
    pilha[topo++] = fila[inicio];
    temp = pesquisa_ARV(a, fila[inicio++]);

    if (temp->esq != NULL && fim < 10) fila[fim++] = temp->esq->info;
    if (temp->dir != NULL && fim < 10) fila[fim++] = temp->dir->info;
  }

  for(int i = 0; i < 11; i++)
  {
    printf("%c ", pilha[i]);
  }

}


int arv_consulta(ARV *a,char c){
  if(arv_vazia(a)){
    return 0;
  }else{
    return a->info==c||arv_consulta(a->esq,c)|| arv_consulta(a->dir,c);
  }
}

ARV* pesquisa_ARV(ARV* p, char c){
  ARV *x = p;
  ARV *ret = NULL;
  if(x->info == c){
    ret = x;
  }
  if(x->esq !=NULL && !ret){
    ret = pesquisa_ARV(x->esq, c);
  }
  if(x->dir !=NULL && !ret){
    ret = pesquisa_ARV(x->dir, c);
  }
  return ret;
}


int main(){
  int opcao=-1,existe=0;
  ARV *a,*a1,*a2,*a3,*a4,*a5,*a6,*a7,*a8,*a9,*a10,*a11,*a12;
  char procura;

  while(opcao!=0){
      printf("======MENU======\n");
      printf("0-Sair\n");
      printf("1-Cria arvore(pre-definida)\n");
      printf("2-Destroi a arvore\n");
      printf("3-Consulta elemento\n");
      printf("4-Imprime a arvore (Pré-ordem)\n");
      printf("5-Imprime a arvore (Em ordem)\n");
      printf("6-Imprime a arvore (Pós-ordem)\n");
      printf("7-Imprime arvore(largura)\n");
      printf("Opcao: ");
      scanf("%d",&opcao);

      switch(opcao){
        case 0:
        break;

        case 1:
          a1=arv_cria('b',arv_criavazia(),arv_criavazia());
          a2=arv_cria('c',arv_criavazia(),arv_criavazia());
          a3=arv_cria('a',arv_criavazia(),arv_criavazia());
          a4=arv_cria('*',a1,a2);
          a6=arv_cria('+',a3,a4);
          a7=arv_cria('a',arv_criavazia(),arv_criavazia());
          a8=arv_cria('b',arv_criavazia(),arv_criavazia());
          a9=arv_cria('+',a7,a8);
          a10=arv_cria('c',arv_criavazia(),arv_criavazia());
          a11=arv_cria('*',a9,a10);
          a=arv_cria('/',a6,a11);
        break;

        case 2:
          a=arv_destroi(a);
          if(a==NULL){
            printf("Arvore vazia!\n");
          }
        break;

        case 3:
        printf("Qual elemento que vc procura na arvore?\n");
        setbuf(stdin,NULL);
        scanf("%c",&procura);
        existe=arv_consulta(a,procura);
        if(existe==1){
          printf("Elemento existe!\n");
        }else{
          printf("Elemento nao existe!\n");
        }
        break;

        case 4:
        imprime_arvore_pre(a);
        break;

        case 5:
        imprime_arvore_em(a);
        break;

        case 6:
        imprime_arvore_pos( a);
        break;

        case 7:
        imprime_porFila(a);
        break;

        default:
        break;
      }
  }

  return 0;
}

