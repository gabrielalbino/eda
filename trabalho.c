//TODO: RETIRAR O VETOR E ADCIONAR P E H

/*Sintese:
**Objetivo: PROGRAMA PARA SIMULAR A MEMÓRIA DE UM COMPUTADOR UTILIZANDO LISTAS DUPLAMENTE ENCADEADA E VETORES
**Entrada: Tamanho do processo, nome do processo, tempo de excecução do processo
**Saída: Exibição da memória do computador
*/

/*
**Includes
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*
**Defines
*/
#define TAM_MEM 1000
#define TAMMENU 50
/*
**Struct para controlar os processos
*/
typedef struct processos{
  char tag;
  int tempo;
  int tamanho;
  int endereco;
  struct processos* proximo;
  struct processos* anterior;
}processos;

/*
**Struct de cabeçalho
*/
typedef struct cabecalho{
  int KBOcupados;
  int nroProcessos;
  struct processos* inicio;
  struct processos* final;
}cabecalho;

/*Sintese:
**Objetivo: Limpar o buffer da entrada
**Parâmetros: Nenhum
**Retorno: Nenhum
*/
void clearBuffer(){
	char c;
	while ((c = getchar()) != '\n' && c != EOF) { }
}

/*Sintese:
**Objetivo: Pausar o console até o usuario pressionar uma tecla
**Parâmetros: Nenhum
**Retorno: Nenhum
*/
void pause(){
	char c;
	clearBuffer();
	printf("Pressione enter para continuar");
	c = getchar();
}

/*Sintese:
**Objetivo:Exibir um menu para o usuário e recolher a opção do menu
**Parâmetros: Nenhum
**Retorno: Opção do usuário
*/
int menu(){
  int o;
  printf("\n\n");
  printf("Escolha uma opção:\n");
  printf("1 - Criar um processo\n");
  printf("2 - Exibir memória\n");
  printf("3 - Passar 1 minuto\n");
  printf("4 - Sair do programa\n");
  scanf("%d", &o);
  return o;
}

/*Sintese:
**Objetivo: Inicializar o cabeçalho
**Parâmetros: Nenhum
**Retorno: Cabeçalho
*/
cabecalho* criaCabecalho(){
  cabecalho* new = malloc(sizeof(cabecalho));
  new->nroProcessos = 0;
  new->KBOcupados = 0;
  new->inicio = NULL;
  new->final = NULL;
  return new;
}

/*Sintese:
**Objetivo: Avaliar se a lista está vazia
**Parâmetros: Cabecalho
**Retorno: 1 se a lista esta vazia, 0 se não está
*/
int listaEstaVazia(cabecalho* p){
  if(p->inicio == NULL && p->final == NULL){
    return 1;
  }
  return 0;
}
/*Sintese:
**Objetivo: Verificar se a tag do processo é unica
**Parâmetros: Elemento a ser inserido, Cabeçalho
**Retorno: 1 se há é unica, 0 se não é
*/
int eUnico(processos* new, cabecalho* p){
  processos* l;
  l = p->inicio;
  if(l!= NULL)
    do{
      if(l->tag == new->tag){
        return 0;
      }
      l = l->proximo;
    }while(l != p->inicio);
    return 1;
}


/*Sintese:
**Objetivo: Verificar se há espaço para alocar um novo processo na memoria
**Parâmetros: Elemento a ser inserido, Cabeçalho
**Retorno: 1 se há espaço, 0 se não há
*/
int temEspaco(processos* new, cabecalho* p){
  if(p->KBOcupados+new->tamanho > TAM_MEM){
    return 0;
  }
  return 1;
}

/*Sintese:
**Objetivo: Inserir um processo no fim da lista encadeada
**Parâmetros: Elemento a ser inserido, Cabeçalho
**Retorno: Nenhum
*/
void insereElementoLista(processos* new, cabecalho* p){
  p->KBOcupados += new->tamanho;
  p->nroProcessos = (p->nroProcessos) + 1;
  if(listaEstaVazia(p)){
    p->inicio = new;
    p->final = new;
    new->proximo = new;
    new->anterior = new;
  }
  else{
    p->final->proximo = new;
    p->inicio->anterior = new;
    new->anterior = p->final;
    new->proximo = p->inicio;
    p->final = new;
  }
}
/*Sintese:
**Objetivo: Verificar se há espaço sequencial para alocar o processo na memoria
**Parâmetros: Elemento a ser inserido, memoria
**Retorno: Endereço inicial do bloco de memoria se há espaço, -1 se não há espaço sequencial.
*/
int procuraEspaco(processos* new, char* memoria){
  int i,j, endInicial = -1, achouEspaco = 0;
  for(i=0; i < TAM_MEM;i++){
    if(i+new->tamanho >= TAM_MEM){
      achouEspaco = 0;
      endInicial = -1;
      i = TAM_MEM;
    }
    else if(memoria[i] == 0){
      achouEspaco = 1;
      endInicial = i;
      for(j=i; j<i+new->tamanho; j++){
        if(memoria[j] != 0){
          achouEspaco = 0;
          endInicial = -1;
          j = i+new->tamanho;
        }
      }
    }
    if(achouEspaco){
      i = TAM_MEM;
    }
  }
  return endInicial;
}

int tamanhoProcesso(char processo, char* memoria, int endereco){
  int i;
  for(i = endereco; i<TAM_MEM;i++){
    if(memoria[i] != processo)
      break;
  }
  return i - endereco;
}

/*Sintese:
**Objetivo: Procurar o proximo espaço da memoria ocupado
**Parâmetros: endereço para começar a procurar, nome do processo a ser ignorado, memoria, ponteiro para tamanho do espaço ocupado
**Retorno: endereço do processo, tamanho do processo
*/
int procuraProximoEspacoNaoNulo(int offset, char pAtual, char* memoria, int* tamanho){
  int i, processo, endereco = -1;
  for( i = offset; i < TAM_MEM; i++){
    if(memoria[i] != 0 && memoria[i] != pAtual){
      processo = memoria[i];
      endereco = i;
      *tamanho = tamanhoProcesso(processo, memoria, endereco);
      break;
    }
  }
  return endereco;
}

/*Sintese:
**Objetivo: Realocar os processos na memoria para organiza-la melhor
**Parâmetros: memoria, cabecalho
**Retorno: Nenhum
*/
void reorganizaMemoria(char* memoria, cabecalho* p){
  processos* procurarProcesso(char procAtual, cabecalho* p);
  int i,tamanho,endereco,j;
  processos *temp;
  char tagAtual = 0;
  for(i=0; i<TAM_MEM; i++){
    if(memoria[i] == 0){
      endereco=procuraProximoEspacoNaoNulo(i, memoria[i], memoria, &tamanho);
      if(endereco != -1){
        temp = procurarProcesso(memoria[endereco], p);
        temp->endereco = i;
        for(j = 0; j < tamanho; j++){
          memoria[i+j] = memoria[endereco+j];
          memoria[endereco+j] = 0;
        }
      }
      else{
        return;
      }
    }
  }
}

/*Sintese:
**Objetivo: Deixar uma string com 50 caracteres
**Parâmetros: string
**Retorno: nenhum
*/

void alinhaString(char* string){
  for(int i = strlen(string); i < TAMMENU-1; i++)
	string[i] = ' ';
  string[TAMMENU-1] = '\0';
}

/*Sintese:
**Objetivo: procura um processo na lista a partir do seu nome
**Parâmetros: Nome do processo, cabeçalho
**Retorno: processo
*/
processos* procurarProcesso(char procAtual, cabecalho* p){
  processos* l;
  l = p->inicio;
  while(l != NULL && l->tag != procAtual){
    l = l->proximo;
  }
  return l;
}

/*Sintese:
**Objetivo: formata a string para exibir informações do processo
**Parâmetros: string para informações básicas, string para informações mais especificas, cabecalho, processo e contador de bytes
**Retorno: nenhum
*/
void fazStringDoProcesso(char* infoBasica, char* info2, cabecalho* p, char procAtual, int counter){
  processos* temp = procurarProcesso(procAtual, p);
  snprintf(infoBasica, TAMMENU, "Processo %c: Ocupa %d bytes",procAtual, counter);
  snprintf(info2, TAMMENU, "%d minutos restantes, Endereco na memoria: 0x%x",temp->tempo, temp->endereco);
  alinhaString(infoBasica);
  alinhaString(info2);
}

/*Sintese:
**Objetivo: Inserir um processo no vetor da memoria
**Parâmetros: Elemento a ser inserido, Cabeçalho, memoria
**Retorno: Nenhum
*/
void adcionaProcessoAoVetor(processos* new, cabecalho* p, char* memoria){
  void imprimeMemoria(cabecalho* p, char* memoria);
  int i, endereco = procuraEspaco(new, memoria);
  if(endereco != -1){
    for(i = endereco; i < endereco+new->tamanho; i++){
      memoria[i] = new->tag;
    }
    new->endereco = endereco;
    insereElementoLista(new, p);
  }
  else{
    reorganizaMemoria(memoria, p);
    adcionaProcessoAoVetor(new, p, memoria); //recursividade
  }
}


/*Sintese:
**Objetivo: Imprimir a memoria do computador
**Parâmetros: cabeçalho, memoria
**Retorno: Nenhum
*/
void imprimeMemoria(cabecalho* p, char* memoria){
  int i,j, counter = 0;
  char procAtual;
  char string[TAMMENU], info[TAMMENU];
  printf("╒");
  for(i = 0; i < TAMMENU-1; i++)
    printf("═");
  printf("╕\n");
  strcpy(string, "Memoria");
  alinhaString(string);
  printf("│%s│\n", string);
 printf("╞");
  for(i = 0; i < TAMMENU-1; i++)
    printf("═");
  printf("╡\n");
  procAtual = memoria[0];
  for(int i = 0; i < TAM_MEM; i++){
    if(memoria[i] == procAtual){
      counter++;
    }
    if(memoria[i] != procAtual || i == TAM_MEM-1){
        if(procAtual != 0){
        fazStringDoProcesso(string,info, p, procAtual,  counter);
        printf("│%s│\n", string);
        printf("│%s│\n", info);
        }
        else{
          snprintf(string, TAMMENU, "Espaco Vazio: %d bytes", counter);
          alinhaString(string);
          printf("│%s│\n", string);
        }
        if(i != TAM_MEM-1){
          printf("├");
           for(j = 0; j < TAMMENU-1; j++)
             printf("─");
           printf("┤\n");
        }
        else{
          printf("╞");
          for(j = 0; j < TAMMENU-1; j++)
             printf("═");
          printf("╡\n");
        }
        counter = 1;
        procAtual = memoria[i];
    }
  }
  strcpy(string, "Fim da memoria");
  alinhaString(string);
  printf("│%s│\n", string);
  printf("╘");
  for(i = 0; i < TAMMENU-1; i++)
      printf("═");
  printf("╛\n");
  pause();
}

/*Sintese:
**Objetivo: Iniciar a memoria com a flag
**Parâmetros: memoria
**Retorno: processo
*/
void iniciaMemoria(char* memoria){
  int i;
  for(i = 0; i < TAM_MEM; i++){
      memoria[i] = 0;
  }
}

/*Sintese:
**Objetivo: Ler um novo processo
**Parâmetros: Nenhum
**Retorno: processo
*/
processos* lerProcesso(){
  processos* new;
  clearBuffer();
  new = malloc(sizeof(processos));
  printf("Insira a sigla do processo (Uma letra): ");
  scanf("%c", &new->tag);
  printf("Insira o tamanho do processo (Em Kbytes): ");
  scanf("%d", &new->tamanho);
  printf("Insira o tempo que o processo ficará rodando (Em minutos): ");
  scanf("%d", &new->tempo);
  new->proximo = NULL;
  new->anterior = NULL;
  new->endereco = -1;
  return new;
}

/*Sintese:
**Objetivo: Encerrar um processo
**Parâmetros:cabecalho,  processo e memoria
**Retorno: nenhum
*/
void encerraProcesso(cabecalho* p, processos* l,char* memoria){
  for(int i = l->endereco; i < l->endereco+l->tamanho; i++){
    memoria[i] = 0;
  }
  if(p->nroProcessos == 1){
    free(p->inicio);
    p->KBOcupados = 0;
    p->inicio = NULL;
    p->final = NULL;
  }
  else{
    if(p->inicio == l){
      p->inicio = l->proximo;
    }
    else if(p->final == l){
      p->final = l->anterior;
    }
    p->KBOcupados = p->KBOcupados - l->tamanho;
    l->anterior->proximo = l->proximo;
    l->proximo->anterior = l->anterior;
    free(l);
  }
  p->nroProcessos = p->nroProcessos-1;
}

/*Sintese:
**Objetivo: Avançar 1 minuto no tempo dos processos
**Parâmetros: cabeçalho e memoria
**Retorno: nenhum
*/
void avancaMinuto(cabecalho* p, char* memoria){
  int contador = 0;
  int limite = p->nroProcessos;
  processos* l = p->inicio;
  do{
    l->tempo = (l->tempo)-1;
    if(l->tempo == 0){
      encerraProcesso(p,l, memoria);
    }
    l=l->proximo;
    contador++;
  }while(contador < limite);
}

/*Sintese:
**Objetivo: Desalocar as variaveis dinâmicamente alocadas
**Parâmetros: cabeçalho
**Retorno: nenhum
*/
void desalocaLista(processos* p, processos* inicio){
  if(p->proximo != inicio)
    desalocaLista(p->proximo, inicio);
  free(p);
}



/*MAIN*/
int main(){
  //declarações
  int o;
  processos* new;
  cabecalho * p;
  char memoria[TAM_MEM];
  FILE* pFile;
  //instruções
  pFile  = fopen("mem.bin", "rb");
  p = criaCabecalho();
  iniciaMemoria(memoria);
  if(pFile != NULL){
    //ler os dados do arquivo
  }
  do{
    o = menu();
    switch(o){
      case 1:
        new = lerProcesso();
        if(temEspaco(new, p)){
          if(eUnico(new, p)){
            adcionaProcessoAoVetor(new, p, memoria);
          }
          else{
            printf("Não pode haver dois processos com o mesmo nome.\n");
          }
        }
        else{
          printf("Não há espaço suficiente na memória para alocar o processo.\n");
          pause();
        }
        break;
      case 2:
        imprimeMemoria(p, memoria);
        break;
      case 3:
        avancaMinuto(p, memoria);
    }
  }while(o != 4);
  //GRAVAR OS DADOS NO ARQUIVO
  desalocaLista(p->inicio, p->inicio);
  free(p);
}
